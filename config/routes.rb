Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'static#home'

  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  get    '/logout',  to: 'sessions#destroy'

  get    '/backstage', to: 'backstage#index'

  get    'pipes/export', to: 'pipes#export'

  resources :sessions
  resources :users
  resources :pipes

end
