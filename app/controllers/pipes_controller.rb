class PipesController < ApplicationController
  before_action :logged_in_user, only: [:index, :update, :edit, :destroy, :show]

  def new
    @pipe = Pipe.new
  end

  def create
    @pipe = Pipe.new(pipe_params)
    if @pipe.save
      flash[:success] = "Pipeline created!"
      render template: "pipes/ok"
    else
      redirect_to root_url
    end
  end

  def edit
    @pipe = Pipe.find(params[:id])
  end

  def update
    @pipe = Pipe.find(params[:id])
    if @pipe.update_attributes(pipe_params)
      flash[:success] = "Pipe updated"
      redirect_to @pipe
    else
      render 'edit'
    end
  end

  def show
    @pipe = Pipe.find(params[:id])
  end

  def destroy
    @pipe = Pipe.find(params[:id])
    @pipe.destroy
    redirect_to backstage_url
  end

  def index
    @pipes = Pipe.paginate(page: params[:page])
  end

  def export
    @pipes = Pipe.all
    respond_to do |format|
      format.csv { send_data @pipes.to_csv }
    end
  end

  private

    def pipe_params
      params.require(:pipe).permit(:email, :description, :status, :ticket, :link,
                                   :groups, :success, :timeframe, :start, :urgency,
                                   :budget, :funding, :additional_funding)
    end
end
