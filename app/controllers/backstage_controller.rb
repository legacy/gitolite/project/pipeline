class BackstageController < ApplicationController
  before_action :logged_in_user
  def index
    @pipes = Pipe.paginate(page: params[:page])
  end
end
