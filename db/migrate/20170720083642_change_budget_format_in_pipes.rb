class ChangeBudgetFormatInPipes < ActiveRecord::Migration[5.1]
  def change
  	change_column :pipes, :budget, :string
  end
end
